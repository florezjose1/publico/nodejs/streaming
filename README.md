# Streaming video


#### Installation and Running App

Version node: `^11.*.*`


* Install modules

`$ npm install
`

* Run app

`$ npm run start`

NOTE: the public folder is ignored, but you must create it and add the videos to play or change the code `routes/video.js:13` to the new path you want to give it.


Made with ♥ by [Jose Florez](www.joseflorez.co)